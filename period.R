decades <- c(1650, 1700, 1750, 1800)
df0$publication_period <- cut(df0$publication_year, breaks = c(1600, 1650, 1700, 1750, 1800), include.lowest = TRUE, right = FALSE)

df <- df0 %>% filter(publication_year >= 1600 & publication_year < 1800) %>%
              filter(publication_place %in% cities) %>%
    group_by(publication_period, publication_place, languages, gatherings) %>%
              filter(gatherings %in% top4.gatherings) %>%
	      arrange(publication_period)

# Top4 languages per place
df <- top_languages_per_place(df, field = "languages", n = 4)

# Calculate proportions for each language per place
# (proportions among top languages!)
prop <- df %>% group_by(publication_place, languages,
                        publication_period, gatherings) %>%
                     tally() %>%
   group_by(publication_place, languages, publication_period) %>%
		     mutate(pct = n/sum(n), n = n) %>%
	arrange(publication_period, publication_place, languages, gatherings)
		     
# Cross plot on publication year vs. page counts in different languages
p <- ggplot(prop, aes(x = publication_period)) +
       geom_bar(aes(y = pct, fill = gatherings),
                stat = "identity", color = "black") +
       labs(x = "Publication period", y = "Fraction (%)") +
       scale_y_continuous(labels = scales::percent) +
       facet_grid(publication_place ~ languages) +
       my_color_scale() +
       my_fill_scale() +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)              

print(p)
