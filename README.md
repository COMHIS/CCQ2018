# CCQ 2019


**Contact** Leo Lahti <leo.lahti@iki.fi>

**Project decription** See https://comhis.github.io/2019_CCQ/

**Figures** To generate figures, run in R: source("main.R")

**Manuscript** Latex sources are in the latex/ folder