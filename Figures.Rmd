---
title: "Figures"
subtitle: "Bibliographic Data Science and the History of the Book (c. 1500-1800)"
author: ""
output: md_document
---


```{r init, echo=FALSE, message=FALSE, warning=FALSE}
library(cowplot)
library(patchwork)
# Theme
theme_set(theme_bw(20))
tmp <- Sys.setlocale(locale="UTF-8")
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(fig.path = "figures/")

#opts_chunk$set(dev="tiff", # png, tiff, postscript
#               dev.args=list(type="cairo"),
#               dpi=300)

opts_chunk$set(dev="cairo_ps", # png, tiff, postscript
               #dev.args=list(type="cairo"),
               dpi=300)

ntoplang <- 3
citylist <- list()
citylist$fennica <- c("Turku")
citylist$kungliga <- c("Stockholm", "Gothenburg", "Lund", "Linköping")
citylist$estc <- names(top(dflist[["estc"]], "publication_place", 11, na.rm = TRUE))
citylist$cerl <- names(top(dflist[["cerl"]], "publication_place", 25, na.rm = TRUE))

library(captioner)
tbls <- captioner(prefix="Table")
figs <- captioner(prefix="Fig.")
subtbls <- captioner(prefix="Supplementary Table")
subfigs <- captioner(prefix="Supplementary Fig.")
```

```{r Fig1_octavo, echo=FALSE, fig.width=13, fig.height=9}
pics <- list()
for (catalog in catalogs) {

  df0 <- dflist[[catalog]]
  cities <- citylist[[catalog]]
  top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))

  df <- df0 %>% filter(!is.na(gatherings))
  df$gatherings <- as.character(df$gatherings)
  df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
  df$gatherings <- factor(df$gatherings, levels = order_gatherings(c(top.gatherings, "Other")))
  dfg <- df

  df <- dfg %>% filter(!is.na(publication_decade) & !is.na(print_area)) %>%
              select(gatherings, publication_decade, print_area) %>%
      	      group_by(publication_decade, gatherings) %>%
	      summarize(print_area = sum(print_area)) %>%
	      group_by(publication_decade) %>%
	      mutate(pct = print_area/sum(print_area)) %>%
	      filter(pct > 0) %>%
	      arrange(publication_decade)

  df$gatherings <- order_gatherings(df$gatherings)

  p <- ggplot(df, aes(x = publication_decade, y = pct, color = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_x_continuous(limits = c(1500, 1800)) +
       scale_y_continuous(label = scales::percent, limits = c(0,1)) +         
       labs(x = "Publication decade", y = "Proportion (%)", title = rename_catalogs(catalog)) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) + 
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  #print(p)

  p <- ggplot(df, aes(x = publication_decade, y = pct, fill = gatherings)) +
       geom_bar(stat="identity", position="stack", color = "black") +
       scale_y_continuous(label = scales::percent) +  
       labs(x = "Publication decade", y = "Proportion (%)", title = rename_catalogs(catalog)) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       guides(fill=guide_legend(title="Gatherings"))       

  # Same legend in all pics       
  legend <- get_legend(p)

  p <- p + guides(color=FALSE, fill = FALSE) 

  #print(p)
  pics[[catalog]] <- p

}

#p <- {{pics[[1]] / pics[[2]]} | {pics[[3]] / pics[[4]]} | legend} + plot_layout(widths = c(3, 3, 2))
#print(p)

#p1 <- plot_grid(pics[[1]], pics[[3]], ncol = 1)
#p2 <- plot_grid(pics[[2]], pics[[4]], ncol = 1)
#p <-  plot_grid(p1, p2 legend, ncol = 3, rel_widths = c(3,3,2))
#print(p)

p1 <- plot_grid(pics[[1]] ,
                pics[[2]] 
		)
p2 <- plot_grid(pics[[3]] ,
                pics[[4]] 
		)		
p3 <-  plot_grid(p1, p2, nrow = 2)
p4 <- plot_grid(p3, legend, ncol = 2, rel_widths = c(6,2))
print(p4)
```

`r figs("octavo","Annual relative print area for common book formats.")`




```{r Fig2_latin, echo=FALSE, fig.width=7.5, fig.height=5}
dfs <- NULL
for (catalog in catalogs) {

  df0 <- dflist[[catalog]]

  df1 <- df0 %>%
              filter(!is.na(language_primary)) %>%
              filter(!is.na(publication_year)) %>%
              select(language_primary, publication_year) %>%
      	      group_by(publication_year, language_primary) %>%
	      tally() %>%
	      group_by(publication_year) %>%
	      mutate(latin.pct = n/sum(n)) %>%
	      filter(language_primary == "Latin") %>%
	      arrange(publication_year)

   df1$catalog <- rep(catalog, nrow(df1))
   dfs <- rbind(dfs, df1)
   # df1$catalog <- factor(df1$catalog, levels = rev(levels(df1$catalog)))

   p <- ggplot(df1, aes(x = publication_year, y = latin.pct)) +
       geom_point() +
       geom_smooth() +
       scale_y_continuous(label = scales::percent,
                          limits = c(0, max(df1$latin.pct))) +
       labs(x = "Publication year",
            y = "Title count (%)",
	    title = paste("Latin share", rename_catalogs(catalog), sep = "/")) 

   # print(p)

}

dfs$catalog <- factor(rename_catalogs(dfs$catalog))
dfs$catalog <- factor(dfs$catalog, levels = rev(c("FNB", "SNB", "ESTC", "HPBD")))
p <- ggplot(dfs, aes(x = publication_year, y = latin.pct, color = catalog, fill = catalog)) +
       #geom_point(aes(shape = catalog)) +
       geom_smooth(aes(linetype = catalog), method = "loess") + 
       scale_y_continuous(label = scales::percent,
                          limits = c(0, max(dfs$latin.pct))) +
       scale_color_manual(values = unname(col.catalog[as.character(levels(dfs$catalog))])) +
       scale_fill_manual(values = unname(col.catalog[as.character(levels(dfs$catalog))])) +
       #scale_color_grey() +
       #scale_fill_grey() +              
       labs(x = "Publication year",
            y = "Title count (%)",
	    title = paste("Latin share", sep = "/")) +
       guides(fill=FALSE, 
              color=guide_legend(title="Catalogue", reverse = TRUE),
	      linetype=guide_legend(title="Catalogue", reverse = TRUE)
	      )

   print(p)
``` 

`r figs("latin_share","Annual title count proportion of books with Latin as the primary (or only) language.")`




```{r Fig3_vernacularization, echo=FALSE, fig.width=11.5, fig.height=7.2, fig.keep="last"}
ntoplang <- 3
top.langs <- list()
for (catalog in catalogs) {
  df0 <- dflist[[catalog]] %>% filter(!is.na(language_primary) &
                                      !is.na(publication_decade) &
				      !is.na(print_area))
  top.langs[[catalog]] <- c(names(top(df0, "language_primary", ntoplang, na.rm = TRUE)), "Other")
}
top.langs <- unique(as.vector(sapply(top.langs, function (x) {unlist(x)})))


for (catalog in catalogs) {

  df0 <- dflist[[catalog]]
  df <- df0 %>% filter(!is.na(language_primary) & !is.na(publication_decade) & !is.na(print_area))

  # Merge less common languages into a single Other category
  df$language_primary <- compress_field(df$language_primary, keep = ntoplang)

  df <- df %>% group_by(publication_decade, language_primary) %>%
             summarise(print_area = sum(print_area, na.rm = TRUE)) %>%
	     arrange(publication_decade)

  # Cross plot on publication decade vs. page counts in different languages
  dfs <- df %>% filter(print_area > 0)
  #dfs$language_primary <- factor(dfs$language_primary, levels = top.langs)

  p <- ggplot(dfs, aes(x = publication_decade, y = print_area,
                    color = language_primary, fill = language_primary)) +
       scale_x_continuous(limits = c(1500, 1800)) + 		    
       scale_y_log10(#limits = 1.4*range(dfs$print_area, na.rm = TRUE),
                     breaks = c(10^c(0:round(log10(max(dfs$print_area))))),
		     labels=scales::trans_format('log10',scales::math_format(10^.x))
       			    ) +  
       geom_point() +
       geom_smooth(fill=NA) +
       labs(x = "Publication decade",
            y = "Print area (sheets)",
	    title = rename_catalogs(catalog)
	    ) +

       
       scale_fill_manual(values = unname(col.languages[as.character(levels(dfs$language_primary))])) +
       scale_color_manual(values = unname(col.languages[as.character(levels(dfs$language_primary))])) +

       guides(color=guide_legend(title="Language"), fill=FALSE) 

  # Same legend in all pics       
  legend <- get_legend(p)
  print(p)
  #p <- p + guides(color=FALSE, fill=FALSE)  
  pics[[catalog]] <- p

}

pp <- plot_grid(pics[[1]],
                pics[[2]],
		pics[[3]],
                pics[[4]], nrow = 2)
				
#p3 <-  plot_grid(p1, p2, nrow = 2)
#p4 <- plot_grid(p3, legend, ncol = 2, rel_widths = c(6,2))
print(pp)


```

`r figs("vernacularization","Changes in print area over time. The three most common languages from each catalogue are included.")`



# Supplementary Figures

```{r SFig1_print_area_share, echo=FALSE, fig.width=18, fig.height=20, fig.keep="last"}
cities <- c("Turku")
catalog <- "fennica"
df0 <- dflist[[catalog]]
top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))

# Merge less common gatherings into a single Other category
df0 <- df0 %>% filter(!is.na(gatherings))
dfs <- df0 %>% filter(publication_place %in% cities)
dfs$gatherings <- compress_field(dfs$gatherings, keep = top.gatherings)

# Cross plot on publication decade vs. page counts in different languages
pics <- list()
for (place in cities) {

  df <- dfs %>% filter(publication_place == place) %>%
  	       filter(!is.na(print_area)) %>%
	       filter(!is.na(publication_decade)) %>%
               select(gatherings, publication_decade, print_area) %>%
               group_by(publication_decade, gatherings) %>%
	       summarise(print_area.total = sum(print_area)) %>%
               group_by(publication_decade) %>%	      
	       mutate(pct.print_area = print_area.total/sum(print_area.total)) %>%
	       filter(pct.print_area > 0) %>%
	       arrange(publication_decade)

  df$gatherings <- order_gatherings(df$gatherings)
  
  p <- ggplot(df, aes(x = publication_decade, y = pct.print_area, color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_x_continuous(limits = c(1500, 1800)) +
       scale_y_continuous(label = scales::percent, limits = c(0,1.1), breaks = seq(0, 1, 0.25)) +         
       labs(x = "Publication decade",
            y = "Proportion (%)",
	    title = paste(rename_catalogs(catalog), place, sep = "/")) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  pics[[place]] <- p
  print(p)
}
pics.fnb <- pics



cities <- c("Frankfurt", "Leipzig", "Halle", "Berlin", "Madrid", "Brussels")
catalog <- "cerl"
df0 <- dflist[[catalog]]
top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))

# Merge less common gatherings into a single Other category
df0 <- df0 %>% filter(!is.na(gatherings))
dfs <- df0 %>% filter(publication_place %in% cities)
dfs$gatherings <- compress_field(dfs$gatherings, keep = 4)

# Cross plot on publication decade vs. page counts in different languages
pics <- list()
for (place in cities) {

  df <- dfs

  df <- df %>% filter(publication_place == place) %>%
  	       filter(!is.na(print_area)) %>%
	       filter(!is.na(publication_decade) & !is.na(print_area)) %>%
               select(gatherings, publication_decade, print_area) %>%
               group_by(publication_decade, gatherings) %>%
	       summarise(print_area.total = sum(print_area)) %>%
               group_by(publication_decade) %>%	      
	       mutate(pct.print_area = print_area.total/sum(print_area.total)) %>%
	       filter(pct.print_area > 0) %>%
	       arrange(publication_decade)

  df$gatherings <- order_gatherings(df$gatherings)
  p <- ggplot(df, aes(x = publication_decade, y = pct.print_area, color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_x_continuous(limits = c(1500, 1800)) +       
       scale_y_continuous(label = scales::percent, limits = c(0, 1.1), breaks = seq(0, 1, 0.25)) +  
       labs(x = "Publication decade",
            y = "Proportion (%)",
	    title = paste(rename_catalogs(catalog), place, sep = "/")) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  pics[[place]] <- p
  print(p)
}
pics.hpbd <- pics



# Supplementary Fig. 19 SNB Print Area share for gatherings per decade/place: Stockholm
# (TÄSSÄ NYT 4 KAUPUNKIA SNB:STÄ. TARVITAANKO KAIKKIA? https://gitlab.com/COMHIS/CCQ2018/raw/master/figures/kungliga_octavo2-1.png) 
cities <- c("Stockholm")
catalog <- "kungliga"
df0 <- dflist[[catalog]]
top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))

# Merge less common gatherings into a single Other category
df0 <- df0 %>% filter(!is.na(gatherings))
dfs <- df0 %>% filter(publication_place %in% cities)
dfs$gatherings <- compress_field(dfs$gatherings, keep = 4)

# Cross plot on publication decade vs. page counts in different languages
pics <- list()
for (place in cities) {

  df <- dfs

  df <- df %>% filter(publication_place == place) %>%
  	       filter(!is.na(print_area)) %>%
	       filter(!is.na(publication_decade) & !is.na(print_area)) %>%
               select(gatherings, publication_decade, print_area) %>%
               group_by(publication_decade, gatherings) %>%
	       summarise(print_area.total = sum(print_area)) %>%
               group_by(publication_decade) %>%	      
	       mutate(pct.print_area = print_area.total/sum(print_area.total)) %>%
	       filter(pct.print_area > 0) %>%
	       arrange(publication_decade)

  df$gatherings <- order_gatherings(df$gatherings)
  p <- ggplot(df, aes(x = publication_decade, y = pct.print_area, color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_x_continuous(limits = c(1500, 1800)) +       
       scale_y_continuous(label = scales::percent, limits = c(0, 1.1), breaks = seq(0, 1, 0.25)) +  
       labs(x = "Publication decade", y = "Proportion (%)", title = paste(rename_catalogs(catalog), place, sep = "/")) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  pics[[place]] <- p
  print(p)
}
pics.snb <- pics


cities <- c("Boston Ma", "Philadelphia Pa", "Dublin", "Edinburgh", "Glasgow", "London", "Oxford", "Cambridge")
catalog <- "estc"
df0 <- dflist[[catalog]]
top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))

# Merge less common gatherings into a single Other category
df0 <- df0 %>% filter(!is.na(gatherings))
dfs <- df0 %>% filter(publication_place %in% cities)
dfs$gatherings <- compress_field(dfs$gatherings, keep = 4)

# Cross plot on publication decade vs. page counts in different languages
pics <- list()
for (place in cities) {

  df <- dfs

  df <- df %>% filter(publication_place == place) %>%
  	       filter(!is.na(print_area)) %>%
	       filter(!is.na(publication_decade) & !is.na(print_area)) %>%
               select(gatherings, publication_decade, print_area) %>%
               group_by(publication_decade, gatherings) %>%
	       summarise(print_area.total = sum(print_area)) %>%
               group_by(publication_decade) %>%	      
	       mutate(pct.print_area = print_area.total/sum(print_area.total)) %>%
	       filter(pct.print_area > 0) %>%
	       arrange(publication_decade)

  df$gatherings <- order_gatherings(df$gatherings)
  p <- ggplot(df, aes(x = publication_decade, y = pct.print_area, color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_x_continuous(limits = c(1500, 1800)) +       
       scale_y_continuous(label = scales::percent, limits = c(0, 1.1), breaks = seq(0, 1, 0.25)) +  
       labs(x = "Publication decade", y = "Proportion (%)", title = paste(rename_catalogs(catalog), place, sep = "/")) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(order_gatherings(df$gatherings))))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  print(p)
  pics[[place]] <- p

}
pics.estc <- pics

legend <- get_legend(pics.estc[[1]])
p <-  plot_grid(
      pics.fnb[[1]] + guides(color=FALSE, fill = FALSE) ,
      pics.snb[[1]] + guides(color=FALSE, fill = FALSE) ,      
      pics.estc[[1]] + guides(color=FALSE, fill = FALSE) ,
      
      pics.estc[[2]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[3]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[4]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[5]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[6]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[7]] + guides(color=FALSE, fill = FALSE) ,
      pics.estc[[8]] + guides(color=FALSE, fill = FALSE) ,            

      pics.hpbd[[1]] + guides(color=FALSE, fill = FALSE) ,
      pics.hpbd[[2]] + guides(color=FALSE, fill = FALSE) ,
      pics.hpbd[[3]] + guides(color=FALSE, fill = FALSE) ,
      pics.hpbd[[4]] + guides(color=FALSE, fill = FALSE) ,
      pics.hpbd[[5]] + guides(color=FALSE, fill = FALSE) ,
      pics.hpbd[[6]] + guides(color=FALSE, fill = FALSE) ,      

      legend,
      
      nrow = 5)
      
print(p)


```

`r subfigs("print_area_share","Annual print area proportion per gathering for selected cities and catalogues.")`




```{r SFig2_vernacular_by_city, echo=FALSE, fig.width=12, fig.height=11.5}
ntoplang <- 3
dfss <- NULL

# Has to be manually set if data updates
langs <- c("Finnish", "Latin", "Swedish", "German", "French", "Undetermined", "English", "Greek Ancient to 1453 ")
langlist <- list()

catalog <- "fennica"
df0 <- dflist[[catalog]]
cities <- "Turku"
# Include top languages per place only

df <- df0 %>% filter(!is.na(language_primary) &
                     !is.na(publication_decade) &
		     !is.na(publication_place) &
		     !is.na(print_area)) %>%
              filter(publication_place %in% cities) 
df <- top_languages_per_place(df, field = "language_primary", n = ntoplang)

df <- df %>% group_by(publication_decade,
	               publication_place,
		       language_primary) %>%
              summarise(print_area = sum(print_area, na.rm = TRUE)) %>%
	      filter(print_area > 0) %>% 
	      arrange(publication_decade)

langlist[[catalog]] <- as.character(unique(df$language_primary))

# Cross plot on publication decade vs. page counts in different languages
pics.fnb <- list()
for (place in cities) {

  dfs <- df %>% filter(publication_place == place)
  dfs$language_primary <- factor(dfs$language_primary, levels = langs)

  p <- ggplot(dfs, aes(x = publication_decade,
       		       y = print_area, 
                       color = language_primary, fill = language_primary)) +
       geom_point() +
       geom_smooth(fill = NA) +
       scale_x_continuous(limits = c(1500, 1800)) + 
       scale_y_log10(limits = 1.4*range(df$print_area, na.rm = TRUE),
                   breaks = 10^(0:round(log10(max(df$print_area, na.rm = TRUE)))),
		   labels=scales::trans_format('log10',scales::math_format(10^.x))		   
       		     ) +       
       labs(x = "Publication decade",
            y = "Print area (sheets)",
	    title = paste(rename_catalogs(catalog), place, sep = "/")) +

       scale_color_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +
       scale_fill_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +

       guides(color=FALSE,
                   fill = FALSE)
		   
  pics.fnb[[place]] <- p
  dfss <- bind_rows(dfss, dfs)

}




catalog <- "kungliga"
df0 <- dflist[[catalog]]
cities <- "Stockholm"
# Include top languages per place only
df <- df0 %>% filter(!is.na(language_primary) &
                     !is.na(publication_decade) &
		     !is.na(publication_place) &
		     !is.na(print_area)) %>%
              filter(publication_place %in% cities)

df <- top_languages_per_place(df, field = "language_primary", n = ntoplang)

df <- df %>% group_by(publication_decade,
	               publication_place,
		       language_primary) %>%
              summarise(print_area = sum(print_area, na.rm = TRUE)) %>%
	      filter(print_area > 0) %>% 
	      arrange(publication_decade)

langlist[[catalog]] <- as.character(unique(df$language_primary))


# Cross plot on publication decade vs. page counts in different languages
pics.snb <- list()
for (place in cities) {
  dfs <- df %>% filter(publication_place == place)
  #dfs$language_primary <- droplevels(dfs$language_primary)
  dfs$language_primary <- factor(dfs$language_primary, levels = langs)
  p <- ggplot(dfs, aes(x = publication_decade,
       		       y = print_area, 
                       color = language_primary, fill = language_primary)) +
       geom_point() +
       geom_smooth(fill = NA) +
       scale_x_continuous(limits = c(1500, 1800)) +        
       scale_y_log10(limits = range(df$print_area, na.rm = TRUE),
                   breaks = 10^(0:round(log10(max(df$print_area, na.rm = TRUE)))),
		   labels=scales::trans_format('log10',scales::math_format(10^.x))		   		   
       		     ) +       
       labs(x = "Publication decade",
            y = "Print area (sheets)",
	    title = paste(rename_catalogs(catalog), place, sep = "/")) +

       scale_color_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +
       scale_fill_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +

       guides(color=FALSE,
                   fill = FALSE)       
  pics.snb[[place]] <- p
  dfs$catalog <- rep(catalog, nrow(dfs))
  dfs$place <- rep(place, nrow(dfs))
  dfs$catalog <- rep(catalog, nrow(dfs))
  dfs$place <- rep(place, nrow(dfs))    
  dfss <- bind_rows(dfss, dfs)

}


catalog <- "cerl"
df0 <- dflist[[catalog]]
cities <- c("Göttingen", "Leiden", "Paris")

# Include top languages per place only
df <- df0 %>% filter(!is.na(language_primary) &
                     !is.na(publication_decade) &
		     !is.na(publication_place) &
		     !is.na(print_area)) %>%
		     filter(publication_place %in% cities) 

df <- top_languages_per_place(df, field = "language_primary", n = ntoplang)

df <- df %>%  
              group_by(publication_decade,
	               publication_place,
		       language_primary) %>%
              summarise(print_area = sum(print_area, na.rm = TRUE)) %>%
	      filter(print_area > 0) %>% 
	      arrange(publication_decade)

langlist[[catalog]] <- as.character(unique(df$language_primary))


# Cross plot on publication decade vs. page counts in different languages
pics.hpbd <- list()
for (place in cities) {
  dfs <- df %>% filter(publication_place == place)
  #dfs$language_primary <- droplevels(dfs$language_primary)
  dfs$language_primary <- factor(dfs$language_primary, levels = langs)
  
  p <- ggplot(dfs, aes(x = publication_decade,
       		       y = print_area, 
                       color = language_primary, fill = language_primary)) +
       geom_point() +
       geom_smooth(fill = NA) +
       scale_x_continuous(limits = c(1500, 1800)) +        
       scale_y_log10(limits = range(df$print_area, na.rm = TRUE),
                   breaks = 10^(0:round(log10(max(df$print_area, na.rm = TRUE)))),
		   labels=scales::trans_format('log10',scales::math_format(10^.x))		   		   
       		     ) +       
       labs(x = "Publication decade",
            y = "Print area (sheets)",
	    title = paste(rename_catalogs(catalog), place, sep = "/")) +

       scale_color_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +
       scale_fill_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +
       
       guides(color=FALSE,
                   fill = FALSE)       
  pics.hpbd[[place]] <- p
  dfs$catalog <- rep(catalog, nrow(dfs))
  dfs$place <- rep(place, nrow(dfs))  
  dfss <- bind_rows(dfss, dfs)  
  #print(p)
}




catalog <- "estc"
df0 <- dflist[[catalog]]
cities <- c("Oxford", "Cambridge", "London")
# Include top languages per place only
df <- df0 %>% filter(!is.na(language_primary) &
                     !is.na(publication_decade) &
		     !is.na(publication_place) &
		     !is.na(print_area)) %>%
              filter(publication_place %in% cities)

df <- top_languages_per_place(df, field = "language_primary", n = ntoplang)

df <- df %>%
              group_by(publication_decade,
	               publication_place,
		       language_primary) %>%
              summarise(print_area = sum(print_area, na.rm = TRUE)) %>%
	      filter(print_area > 0) %>% 
	      arrange(publication_decade)

langlist[[catalog]] <- as.character(unique(df$language_primary))

# Cross plot on publication decade vs. page counts in different languages
pics.estc <- list()
for (place in cities) {
  dfs <- df %>% filter(publication_place == place)
  dfs$language_primary <- factor(dfs$language_primary, levels = langs)
  p <- ggplot(dfs, aes(x = publication_decade,
       		       y = print_area, 
                       color = language_primary, fill = language_primary)) +
       geom_point() +
       geom_smooth(fill = NA) +
       scale_x_continuous(limits = c(1500, 1800)) + 
       scale_y_log10(limits = range(df$print_area, na.rm = TRUE),
                   breaks = 10^(0:round(log10(max(df$print_area, na.rm = TRUE)))),
		   labels=scales::trans_format('log10',scales::math_format(10^.x))		   		   
       		     ) +       
       labs(x = "Publication decade",
            y = "Print area (sheets)",
            title = paste(rename_catalogs(catalog), place, sep = "/")) +

       scale_color_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +
       scale_fill_manual(values = col.languages[as.character(levels(dfs$language_primary))]) +

       guides(color=FALSE,
                   fill = FALSE)
		   
  pics.estc[[place]] <- p

  dfs$catalog <- rep(catalog, nrow(dfs))
  dfs$place <- rep(place, nrow(dfs))  
  dfss <- bind_rows(dfss, dfs)

}


p0 <- ggplot(dfss, aes(x = publication_decade,
       		       y = print_area, 
                       color = language_primary,
		       fill = language_primary)) +
       geom_point() +
       scale_color_manual(values = col.languages[as.character(levels(dfss$language_primary))]) +
       scale_fill_manual(values = col.languages[as.character(levels(dfss$language_primary))]) +       
       guides(color=guide_legend(title="Language"),
                   fill = FALSE)
legend <- get_legend(p0)		   
  

pp <- plot_grid(
          pics.fnb[[1]], 
          pics.snb[[1]],
          pics.estc[[1]],
	  pics.estc[[2]],
	  pics.estc[[3]],
	  pics.hpbd[[1]],
	  pics.hpbd[[2]],
	  pics.hpbd[[3]],
	  legend,
	  nrow = 3)

print(pp)


```

`r subfigs("vernacular_by_city","Annual print area for primary languages in selected cities. The most common languages in each place are included.")`



```{r SFig3_usa, echo=FALSE, fig.width=16, fig.height=4, fig.keep="last"}
#  USA case ESTC: Yhteensä 9 kuvaa,
#  3 per kieli (englanti, latina sekä muut)
#  3 aluetta (Lontoo, Muu UK, Pohjois-Amerikka)
# primary_language considered
pics <- list()
places <- c("London", "Other England", "USA")
langs <- c("English", "Latin")

catalog <- "estc"
df0 <- dflist[[catalog]]
df <- df0 %>% filter(!is.na(gatherings))
df$gatherings <- compress_field(df$gatherings, top.gatherings, "Other")
df$language_primary <- compress_field(df$language_primary, langs, "Other")
langs <- as.character(unique(df$language_primary))

df <- df %>% filter(publication_place %in% c("London") |
                     (publication_country == "England" & !publication_place == "London") |
		     publication_country == "USA")
df$publication_place <- as.character(df$publication_place)
df$publication_place[which(df$publication_country == "USA")] <- "USA"
df$publication_place[which(df$publication_country == "England" & !df$publication_place == "London")] <- "Other England"
df$publication_place <- factor(df$publication_place, levels = c("London", "Other England", "USA"))

dfs <- df %>% filter(publication_place %in% places & language_primary %in% langs)
dfss <- NULL

for (place in places) {

  for (lang in langs) {  

    df <- dfs %>% select(gatherings, publication_decade, publication_place, language_primary) %>%
                  filter(!is.na(publication_decade)) %>%
      	          filter(publication_place == place) %>%
      	          filter(language_primary == lang) %>%		
      	          group_by(publication_decade, publication_place, language_primary, gatherings) %>%
	          tally() %>%
	          group_by(publication_decade) %>%
	          mutate(pct = n/sum(n)) %>%
	          arrange(publication_decade)

    df$gatherings <- order_gatherings(df$gatherings)
    df$place <- rep(place, nrow(df))
    df$lang <- rep(lang, nrow(df))
    dfss <- bind_rows(dfss, df)
    
    p <- ggplot(df, aes(x = publication_decade, y = pct,
                        color = gatherings,
		        fill = gatherings)) +

       geom_bar(aes(y = pct, fill = gatherings),
                stat = "identity", color = "black") +
       scale_x_continuous(limits = c(1500, 1800)) +
       scale_y_continuous(label = scales::percent, limits = c(0, 1)) +                
       labs(x = "Publication decade", y = "Proportion (%)", title = paste(place, lang, sep = "/")) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +        
       guides(fill=guide_legend(title="Gatherings"), color = FALSE)
		   
    pics[[paste(place, lang, sep = "/")]] <- p

  }
}


legend <- get_legend(pics[[1]])
library(patchwork)
#p <-  plot_grid(
#      pics[[1]] + guides(color=FALSE, fill = FALSE),
#      pics[[2]] + guides(color=FALSE, fill = FALSE),
#      pics[[3]] + guides(color=FALSE, fill = FALSE),
#      pics[[4]] + guides(color=FALSE, fill = FALSE),
#      #pics[[5]] + guides(color=FALSE, fill = FALSE),
#      #pics[[6]] + guides(color=FALSE, fill = FALSE),
#      #pics[[7]] + guides(color=FALSE, fill = FALSE),
#      #pics[[8]] + guides(color=FALSE, fill = FALSE),
#      #pics[[9]] + guides(color=FALSE, fill = FALSE),
#      legend,                        
#      nrow = 1)


p1 <- plot_grid(pics[[1]] + guides(color=FALSE, fill = FALSE),
                pics[[2]] + guides(color=FALSE, fill = FALSE),
                pics[[3]] + guides(color=FALSE, fill = FALSE),
		legend,
		nrow = 1,
		rel_widths = c(4,4,4,2)
		)
#p3 <-  plot_grid(p1, legend, nrow = 2, rel_widths = c(6,2))
print(p1)
```

`r subfigs("general_usa","Proportion of gatherings by title count for English, Latin, and other languages in London according to ESTC.")`


```{r SFig4_octavo_languages, echo=FALSE, fig.width=13, fig.height=8, fig.keep = "last"}
langs <- c("Finnish", "Swedish", "Latin", "Other", "German", "English", "French")
pics <- list()
dfss <- NULL
for (catalog in catalogs) {

  df0 <- dflist[[catalog]]

  df <- df0 %>% filter(gatherings == "Octavo")

  df$language_primary <- compress_field(df$language_primary, keep = ntoplang)
  
  df <- df %>% filter(!is.na(publication_decade) &
                      !is.na(language_primary) &
		      !is.na(print_area)) %>%
              select(publication_decade, language_primary, print_area) %>%
      	      group_by(publication_decade, language_primary) %>%
	      summarize(print_area = sum(print_area)) %>%
	      group_by(publication_decade) %>%
	      mutate(pct = print_area/sum(print_area)) %>%
	      filter(pct > 0) %>%
	      arrange(publication_decade)

  dfs <- df

  p <- ggplot(dfs, aes(x = publication_decade,
                      y = pct,
                      color = language_primary,
		      fill = language_primary)) +
       geom_point() +
       geom_smooth(fill = NA) +
       scale_x_continuous(limits = c(1500, 1800)) +    
       scale_y_continuous(label = scales::percent, limit = c(0,1)) +  
       labs(x = "Publication decade",
            y = "Proportion (%)",
	    title = rename_catalogs(catalog)) +
       
        scale_fill_manual(values = unname(col.languages[as.character(levels(dfs$language_primary))])) +
       scale_color_manual(values = unname(col.languages[as.character(levels(dfs$language_primary))])) +

       guides(color=guide_legend(title="Language"), fill=FALSE)

  print(p)
  pics[[catalog]] <- p

}

p1 <- plot_grid(pics[[1]], 
                pics[[2]],
		pics[[3]],
                pics[[4]], nrow = 2)
print(p1)
```

`r subfigs("octavo_languages","Relative print area per language in octavo books. The most common languages for each catalogue are shown.")`
