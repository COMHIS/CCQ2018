---
title: "CCQ 2018 Figures and Tables: ESTC"
author: "`r author`"
date: "`r Sys.Date()`"
output: md_document
---

# CCQ 2018 / ESTC

```{r init, echo=FALSE, message=FALSE, warning=FALSE, cache=FALSE}
# Theme
theme_set(theme_bw(20))
tmp <- Sys.setlocale(locale="UTF-8")
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(fig.path = "figures/estc_")

# Table of multilingual docs
library(stringr)
multilingual <- df0 %>% filter(str_detect(languages, "Swedish") &
                               str_detect(languages, "Latin")) %>%
       	       arrange(languages, publication_year, title) %>%
	       select(languages, publication_year, title)
write.table(multilingual,
            file = "multilingual.csv",
	    sep = "\t", quote = FALSE, row.names = FALSE)


# knitr::opts_chunk$set(fig.path = "figure-2016-manuscript/", dev="CairoPNG")
df0$gatherings <- map_gatherings(df0$gatherings)
# Global variables
top.gatherings <- names(top(df0, "gatherings", 4, na.rm = TRUE))
#cities <- names(top(df0, "publication_place", 20, na.rm = TRUE))
cities <- names(top(df0, "publication_place", 11, na.rm = TRUE))
ntoplang <- 3
top.languages <- names(top(df0, "language_primary", ntoplang, na.rm = TRUE))
```


## ESTC General: Latin share

Annual title count percentage of books in Latin per year for ESTC
(missing mappings ignored). In ESTC we now have only primary language
information listed for each document. **No multilingual information in
the present ESTC workflow**. Could be added but will take time.

```{r general_languages, echo=FALSE, fig.width=6, fig.height=5, out.width="500px"}
df1 <- df0 %>%
              filter(!is.na(language_primary)) %>%
              filter(!is.na(publication_year)) %>%
              select(language_primary, publication_year) %>%
      	      group_by(publication_year, language_primary) %>%
	      tally() %>%
	      group_by(publication_year) %>%
	      mutate(latin.pct = n/sum(n)) %>%
	      filter(language_primary == "Latin") %>%
	      arrange(publication_year)

p1 <- ggplot(df1, aes(x = publication_year, y = latin.pct)) +
       geom_point() +
       geom_smooth() +
       scale_y_continuous(label = scales::percent,
                          limits = c(0, max(df1$latin.pct))) +
       labs(x = "Publication year",
            y = "Title count (%)",
	    title = "Latin only/primary")

print(p1)
``` 


## Octavo 

Annual paper consumption for different book formats: total and relative.

```{r octavo, echo=FALSE, fig.width=8, fig.height=5, fig.show="keep", out.width="550px"}
df <- df0 %>% filter(!is.na(gatherings))
df$gatherings <- as.character(df$gatherings)
df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
df$gatherings <- factor(df$gatherings, levels = c(top.gatherings, "Other"))
dfg <- df

dfs <- df %>%
          group_by(publication_year, gatherings) %>%
          summarise(paper = sum(paper, na.rm = TRUE)) %>%
	      arrange(publication_year) %>%
	      filter(paper > 0)
dfs$gatherings <- droplevels(dfs$gatherings)

# Cross plot on publication year vs. page counts in different languages  
p.tot <- ggplot(dfs, aes(x = publication_year, y = paper,
                    color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       labs(x = "Publication year",
            y = "Paper (sheets)",
	    guide = "Gatherings") +
       scale_y_log10(limits = range(dfs$paper, na.rm = TRUE), breaks = 10^(0:round(log10(max(dfs$paper, na.rm = TRUE))))) + 
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(dfs$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(dfs$gatherings)))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)



df <- dfg %>% filter(!is.na(publication_year) & !is.na(paper)) %>%
              select(gatherings, publication_year, paper) %>%
      	      group_by(publication_year, gatherings) %>%
	      summarize(paper = sum(paper)) %>%
	      group_by(publication_year) %>%
	      mutate(pct = paper/sum(paper)) %>%
	      filter(pct > 0) %>%
	      arrange(publication_year)

p.rel <- ggplot(df, aes(x = publication_year, y = pct, color = gatherings)) +
       geom_point() +
       geom_smooth() +
       #scale_y_log10(limits = range(df$n)) +
       scale_y_continuous(label = scales::percent, limits = c(0,1)) +  
       labs(x = "Publication year", y = "Fraction (%)") +
       #my_color_scale() +
       #my_fill_scale() +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) + 

       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       


print(p.tot)
print(p.rel)
```


## Octavo: paper share by place

Paper share for gatherings per year/place.

```{r gatherings_paper_by_place, echo=FALSE, fig.width=30, fig.height=15}
dfs <- df0 %>% filter(publication_place %in% cities) %>%
               filter(gatherings %in% top.gatherings) %>%
               group_by(publication_year, publication_place, gatherings) %>%
	       arrange(publication_year) %>%
	       filter(!is.na(gatherings))
dfs$gatherings <- as.character(dfs$gatherings)
dfs$gatherings[which(!dfs$gatherings %in% top.gatherings)] <- "Other"
dfs$gatherings <- factor(dfs$gatherings, levels = c(top.gatherings, "Other"))

# Cross plot on publication year vs. page counts in different languages
pics <- list()
for (place in cities) {

  df <- dfs

  df <- df %>% filter(publication_place == place) %>%
  	       filter(!is.na(paper)) %>%
	       filter(!is.na(publication_year) & !is.na(paper)) %>%
               select(gatherings, publication_year, paper) %>%
               group_by(publication_year, gatherings) %>%
	       summarise(paper.total = sum(paper)) %>%
               group_by(publication_year) %>%	      
	       mutate(pct.paper = paper.total/sum(paper.total)) %>%
	       filter(pct.paper > 0) %>%
	       arrange(publication_year)

  p <- ggplot(df, aes(x = publication_year, y = pct.paper, color = gatherings, fill = gatherings)) +
       geom_point() +
       geom_smooth() +
       scale_y_continuous(label = scales::percent, limits = c(0, 1)) +  
       labs(x = "Publication year", y = "Fraction (%)", title = place) +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +        
       guides(color=guide_legend(title="Gatherings"),
                   fill = FALSE)       

  pics[[place]] <- p

}

grid.arrange(
  pics[[1]], pics[[2]], pics[[3]], pics[[4]],
  pics[[5]], pics[[6]], pics[[7]], pics[[8]],
  pics[[9]], pics[[10]], pics[[11]], 
  nrow = 3
)
```



### USA Gatherings

Based on title count.

```{r general_usa, echo=FALSE, fig.width=24, fig.height=15}
#  USA case ESTC: Yhteensä 9 kuvaa,
#  3 per kieli (englanti, latina sekä muut)
#  3 aluetta (Lontoo, Muu UK, Pohjois-Amerikka)
# primary_language considered
pics <- list()
places <- c("London", "Other England", "USA")
langs <- c("English", "Latin", "Other")

df <- df0 %>% filter(!is.na(gatherings))
df$gatherings <- as.character(df$gatherings)
df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
df$gatherings <- factor(df$gatherings, levels = c(top.gatherings, "Other"))
df <- df %>% filter(publication_place %in% c("London") |
                     (publication_country == "England" & !publication_place == "London") |
		     publication_country == "USA")
df$publication_place <- as.character(df$publication_place)
df$publication_place[which(df$publication_country == "USA")] <- "USA"
df$publication_place[which(df$publication_country == "England" & !df$publication_place == "London")] <- "Other England"
df$publication_place <- factor(df$publication_place, levels = c("London", "Other England", "USA"))

df$language_primary <- as.character(df$language_primary)
df$language_primary[which(!df$language_primary %in% c("English", "Latin"))] <- "Other"
df$language_primary <- factor(df$language_primary, levels = langs)

dfs <- df %>% filter(publication_place %in% places & language_primary %in% langs)

for (place in places) {

  for (lang in langs) {  

    df <- dfs %>% select(gatherings, publication_decade, publication_place, language_primary) %>%
                  filter(!is.na(publication_decade)) %>%
      	          filter(publication_place == place) %>%
      	          filter(language_primary == lang) %>%		
      	          group_by(publication_decade, publication_place, language_primary, gatherings) %>%
	          tally() %>%
	          group_by(publication_decade) %>%
	          mutate(pct = n/sum(n)) %>%
	          arrange(publication_decade)

    p <- ggplot(df, aes(x = publication_decade, y = pct,
                        color = gatherings,
		        fill = gatherings)) +

       geom_bar(aes(y = pct, fill = gatherings),
                stat = "identity", color = "black") +
       #geom_point() +
       #geom_smooth() +
       scale_y_continuous(label = scales::percent, limits = c(0, 1)) +         
       labs(x = "Publication decade", y = "Fraction (%)", title = paste(place, lang, sep = "/")) +
       my_color_scale() +
       my_fill_scale() +        
       guides(fill=guide_legend(title="Gatherings"), color = FALSE)
		   
    pics[[paste(place, lang, sep = "/")]] <- p

  }
}

grid.arrange(pics[[1]], pics[[2]], pics[[3]],
             pics[[4]], pics[[5]], pics[[6]],
             pics[[7]], pics[[8]], pics[[9]],
	     nrow = 3)
```




## Vernacularization

Paper consumption in the top languages in the whole catalog.

```{r vernacularization_overall, echo=FALSE, fig.width=9, fig.height=6}
df <- df0 %>% filter(!is.na(language_primary) & !is.na(publication_year) & !is.na(paper))
df$language_primary <- as.character(df$language_primary)
df$language_primary[which(!df$language_primary %in% top.languages[1:ntoplang])] <- "Other"
df$language_primary <- factor(df$language_primary, levels = c(top.languages, "Other"))

df <- df %>%
              group_by(publication_year, language_primary) %>%
              summarise(paper = sum(paper, na.rm = TRUE)) %>%
	      arrange(publication_year)

# Cross plot on publication year vs. page counts in different languages
  dfs <- df %>% filter(paper > 0)
  dfs$language_primary <- droplevels(dfs$language_primary)
  p <- ggplot(dfs, aes(x = publication_year, y = paper,
                    color = language_primary)) +
       scale_y_log10(limits = range(dfs$paper, na.rm = TRUE)) +  
       geom_point() +
       geom_smooth() +
       labs(x = "Publication year", y = "Paper (sheets)") +
       my_color_scale() +
       guides(color=guide_legend(title="Language"))       
print(p)
```




## Vernacularizations by city

Annual share of languages in selected cities.

**Top-3 languages included for each place / NOTE: color codings change per figure**

```{r vernacular_by_city, echo=FALSE, fig.width=20, fig.height=17, fig.show="first"}
# Include top languages per place only
df <- df0 %>% filter(!is.na(language_primary) &
                     !is.na(publication_year) &
		     !is.na(publication_place) &
		     !is.na(paper)) %>%
              filter(publication_place %in% cities) %>%
              group_by(publication_year,
	               publication_place,
		       language_primary) %>%
              summarise(paper = sum(paper, na.rm = TRUE)) %>%
	      filter(paper > 0) %>% 
	      arrange(publication_year)

source("funcs.R")
df <- top_languages_per_place(df, field = "language_primary", n = ntoplang)

# Cross plot on publication year vs. page counts in different languages
pics <- list()
for (place in sort(unique(df$publication_place))) {
  dfs <- df %>% filter(publication_place == place)
  dfs$language_primary <- droplevels(dfs$language_primary)

  p <- ggplot(dfs, aes(x = publication_year,
       		       y = paper, 
                       color = language_primary)) +
       geom_point() +
       geom_smooth() +
       scale_y_log10(limits = range(dfs$paper, na.rm = TRUE),
                     breaks = 10^(0:round(log10(max(dfs$paper, na.rm = TRUE))))
       		     ) +       
       labs(x = "Publication year", y = "Paper (sheets)", title = place) +
       my_color_scale() +
       my_fill_scale() +        
       guides(color=guide_legend(title="Language"),
                   fill = FALSE)       
  pics[[place]] <- p
}

library(cowplot)
prow <-  plot_grid(
  pics[[1]], pics[[2]], pics[[3]], pics[[4]], pics[[5]],
  pics[[6]], pics[[7]], pics[[8]], pics[[9]], pics[[10]],
  pics[[11]], 
  nrow = 4)

print(prow)
#p <- plot_grid(prow, legend, rel_widths = c(20, 6))
#print(p)
```





## Combined 2

Shares of different book formats (title count!) in different languages
for publication periods of 50 years. Top languages shown for each
place.


```{r combined2, echo=FALSE, fig.width=15, fig.height=30}
df0$publication_period <- cut(df0$publication_year,
                              breaks = c(1600, 1650, 1700, 1750, 1799),
			      include.lowest = TRUE,
			      right = FALSE)

df0$publication_period <- as.character(df0$publication_period)
df0$publication_period[which(df0$publication_period == "[1.6e+03,1.65e+03)")] <- "1600-1649"
df0$publication_period[which(df0$publication_period == "[1.65e+03,1.7e+03)")] <- "1650-1699"
df0$publication_period[which(df0$publication_period == "[1.7e+03,1.75e+03)")] <- "1700-1749"
df0$publication_period[which(df0$publication_period == "[1.75e+03,1.8e+03]")] <- "1750-1799"
df0$publication_period <- factor(df0$publication_period, levels = c("1600-1649", "1650-1699", "1700-1749", "1750-1799"))

df <- df0 %>% filter(!is.na(gatherings))
df$gatherings <- as.character(df$gatherings)
df$gatherings[which(!df$gatherings %in% top.gatherings)] <- "Other"
df$gatherings <- factor(df$gatherings, levels = c(top.gatherings, "Other"))

df <- df %>% filter(publication_year >= 1600 & publication_year < 1800) %>%
              filter(publication_place %in% cities) %>%
    group_by(publication_period, publication_place, languages, gatherings) %>%
              filter(gatherings %in% top.gatherings) %>%
	      arrange(publication_period)

# Top languages per place
df <- top_languages_per_place(df, field = "languages", n = ntoplang)

# Calculate proportions for each language per place
# (proportions among top languages!)
prop <- df %>% group_by(publication_place, languages,
                        publication_period, gatherings) %>%
                     tally() %>%
   group_by(publication_place, languages, publication_period) %>%
		     mutate(pct = n/sum(n), n = n) %>%
	arrange(publication_period, publication_place, languages, gatherings)
		     
# Cross plot on publication year vs. page counts in different languages
p <- ggplot(prop, aes(x = publication_period)) +
       geom_bar(aes(y = pct, fill = gatherings),
                stat = "identity", color = "black") +
       labs(x = "Publication period", y = "Fraction (%)") +
       scale_y_continuous(labels = scales::percent) +
       facet_grid(publication_place ~ languages) +
       #my_color_scale() +
       #my_fill_scale() +
       scale_fill_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +
       scale_color_manual(values = unname(col.gatherings[as.character(tolower(levels(df$gatherings)))])) +                      
       guides(fill=guide_legend(title="Gatherings"),
                   color = FALSE) +
       theme(axis.text.x = element_text(angle = 90))		  

print(p)
```

The exact numbers are shown in the table:

```{r combined2_table, echo=FALSE, fig.width=25, fig.height=10}
prop$pct <- 100 * prop$pct 
names(prop) <- map_fieldnames(names(prop))
kable(prop, digits = 1)
```


## ESTC Top-100 Publication places 1700-1799

Top 100 publication places in ESTC for the total number of books
published in 1700-1799. **The missing places have been omitted in
percentage calculations.**

```{r toppub, echo=FALSE, fig.width=10, fig.height=15}
df <- df0 %>% filter(publication_year >= 1700) %>%
      	      filter(publication_year <= 1799) %>%
	      select(publication_place)
tab <- top(df, "publication_place", 100, output = "data.frame",
               include.rank = TRUE)
names(tab) <- map_fieldnames(names(tab))
kable(tab, digits = 1)
```



## Duodecimo

See [CSV on duodecimo in ESTC](duodecimo.csv).

```{r duodecimo_csv, echo=FALSE, fig.width=10, fig.height=15}
df <- df0 %>% filter(gatherings == "Duodecimo" & publication_year == 1799) %>%
              arrange(publication_year) %>%
	      select(publication_year, publication_place, pagecount, title)
names(df) <- map_fieldnames(names(df))
kable(df)
write.table(df, quote = F, row.names = F, sep = "\t", file = "duodecimo.csv")
```

